select
		count(trip_start_station_name) as number_of_trips,
		trip_start_station_name,
		trip_end_station_name

from trips_data_all_q
where member_type = 'Subscriber' and trip_start_station_name != trip_end_station_name
group by trip_start_station_name, trip_end_station_name
order by number_of_trips desc
limit 10
