# Data Analysis Portfolio

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Project 1: [Insight in bike usage between Subscriber and Non-recurring customers](https://gitlab.com/lesthersk/data-analysis-projects/-/tree/main/Case%20Study%201)

TLDR: The marketing team is trying to launch a new campaign to convert one time user to subscribers as they are
more profitable, the task is to gain insights on how both types of customers use the service.
The major differences are that Subscribers use the service for short trips on average, usually in areas that have 
bus stops, train stations, a few commercial areas, which suggest that they could be using the service for daily work commuting,
with peak usage during weekdays. In contrast to one time user that use it for longer time periods, usually near restaurants, 
peers, and parks, with higher usage during fridays, and weekends. However, more data is needed to further corroborate the findings.


## Project 2: TODO

## Roadmap
For now I just complete a case study included in the program. In a next iteration I will include a data science type project as
the available datasets in Kaggle have already been studied many times or showcasing all my skills becomes a bit difficult.


## Contributing
If you found something I missed, or could improve, feel free to open a request so I can take a look =)


## Acknowledgment
Big shout out to folks at Coursera and Google for creating the [Data Analytics Certificate](https://www.coursera.org/professional-certificates/google-data-analytics), without it, my path into Data Analytics would have been a lot different!

## License
Released under GNU Lesser General Public License v3.0. 

## Project status
On active development
